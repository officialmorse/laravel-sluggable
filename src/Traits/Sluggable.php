<?php

namespace OfficialMorse\Sluggable\Traits;

use OfficialMorse\Sluggable\Scopes\SluggableScope;
use OfficialMorse\Sluggable\Observers\SluggableObserver;

trait Sluggable {


    /**
     * Boot the soft deleting trait for a model.
     *
     * @return void
     */
    public static function bootSluggable()
    {
        static::addGlobalScope(new SluggableScope);
        static::observe(new SluggableObserver);
    }

    /**
     * get the name of the slug column.
     *
     * @return string
     */
    public function getSlugKeyName()
    {
        if (property_exists($this, 'slugKeyName')) {
            return $this->slugKeyName;
        }
        return 'slug';
    }

    /**
     * get the value of the slug field
     *
     * @return string
     */
    public function getSlugValue()
    {
        return $this->getAttribute($this->getSlugKeyName());
    }

    /**
     * Get the column that should be converted to a slug
     * @return string
     * @throws \UnexpectedValueException
     */
    public function getSluggableColumnName()
    {
        if (property_exists($this, 'slugFrom')) {
            return $this->slugFrom;
        }
        throw new \UnexpectedValueException("The sluggable column is not set on the model");
    }

    /**
     * Get the value of the sluggable column
     * @return string
     * @throws \UnexpectedValueException
     */
    public function getSluggableValue()
    {
        return $this->getAttribute($this->getSluggableColumnName());
    }

}
