<?php

namespace OfficialMorse\Sluggable\Observers;

use Illuminate\Database\Eloquent\Model;

/**
* Observer for sluggable models
*/
class SluggableObserver
{

    public static $slugSeperator = '-';

    /**
     * Fired when the record is about to be created
     * @param  \Illuminate\Database\Eloquent\Model $model   The model being created
     * @return void
     */
    public function creating(Model $model)
    {
        $this->model = $model;
        $cleanSlug = str_slug($this->model->getSluggableValue(), Static::$slugSeperator);
        $uniqueSlug = $cleanSlug;
        $i = 0;
        while ($this->model->newQueryWithoutScopes()->where($this->model->getSlugKeyName(), '=', $uniqueSlug)->first()) {
            $i++;
            $uniqueSlug = $cleanSlug.Static::$slugSeperator.$i;
        }
        $this->model->slug = $uniqueSlug;
    }

}
