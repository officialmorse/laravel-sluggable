<?php

namespace OfficialMorse\Sluggable\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class SluggableScope implements Scope
{
    /**
     * All of the extensions to be added to the builder.
     *
     * @var array
     */
    protected $extensions = ['WhereSlug', 'FindBySlug', 'FindBySlugOrFail'];

    /**
     * Apply the scope to a given Eloquent query builder.
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        //
    }

    /**
     * Extend the query builder with the needed functions.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    public function extend(Builder $builder)
    {
        foreach ($this->extensions as $extension) {
            $this->{"add{$extension}"}($builder);
        }
    }

    /**
     * Add the whereSlug extension
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function addWhereSlug(Builder $builder) {
        $builder->macro('whereSlug', function (Builder $builder, $slug) {
            return $builder->where($builder->getModel()->getSlugKeyName(), '=', $slug);
        });
    }

    /**
     * Add extension to find a model by its slug.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static[]|static|null
     */
    public static function addFindBySlug(Builder $builder) {
        $builder->macro('findBySlug', function (Builder $builder, $slug, $columns = ['*']) {
            return $builder->whereSlug($slug)->first($columns);
        });
    }

    /**
     * Add extension to find a model by its slug or throw an exception.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public static function addFindBySlugOrFail(Builder $builder) {
        $builder->macro('findBySlugOrFail', function (Builder $builder, $slug, $exception = null, $columns = ['*']) {
            try {
                return $builder->whereSlug($slug)->firstOrFail($columns);
            } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                if (!is_null($exception)) {
                    throw new $exception();
                }
                throw $e;
            }
        });
    }

}
